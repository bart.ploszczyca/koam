using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D playerRigidbody;
    private BoxCollider2D boxCollider2d;
    private Animator animator;

    public float horizontalValue;
    public float movementSpeed;
    public float jumpForce;

    public bool isFacingRight;
    public bool isGrounded;
    public bool isSliding;

    public Transform attackPoint;
    public LayerMask enemyLayer;
    public float attackRange;
    public int attackDamage;
    public float attackRate;
    float nextAttackTime = 0f;

    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        boxCollider2d = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();

        isFacingRight = true;
        isSliding = false;
    }

    void Update()
    {
        MovementMechanics();

        if (!isSliding)
            GroundCheck();

        

        if (Time.time >= nextAttackTime)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Attack();
                nextAttackTime = Time.time + 1f / attackRate;
            }
        }

        if (isGrounded && horizontalValue != 0 && Input.GetKeyDown("s"))
        {
            Slide();
        }
    }
    void MovementMechanics()
    {
        FrezeMovementIfAttack();

        SlideMechanic();

        JumpMechanic();

        RunningMechanic();

        TurningMechanic();
    }
    void GroundCheck()
    {
        //gets public variable from player child with his own collider
        //and makes it available variable in this code
        if (transform.GetChild(0).GetComponent<GroundCheck>().isGrounded == true)
            isGrounded = true;
        else
            isGrounded = false;
    }
    void JumpMechanic()
    {
        //jumping mechanic
        if (Input.GetKeyDown(KeyCode.W) && isGrounded)
        {
            
            playerRigidbody.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
            isGrounded = false;
        }

        //Animator jump and fall setting code
        if (!isGrounded)
            animator.SetBool("Jump", true);
        else
            animator.SetBool("Jump", false);

        //Animator yVelocity float changer
        animator.SetFloat("yVelocity", playerRigidbody.velocity.y);
    }
    void Attack()
    {
        animator.SetTrigger("Attack");

        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayer);
        foreach (Collider2D enemy in hitEnemies)
        {
            enemy.GetComponent<Enemy>().TakeDamage(attackDamage);
            Debug.Log("We hit" + enemy.name);
        }
    }
    void FrezeMovementIfAttack()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Player_Attack1") | animator.GetCurrentAnimatorStateInfo(0).IsName("Player_Attack2") | animator.GetCurrentAnimatorStateInfo(0).IsName("Player_Attack3") | animator.GetCurrentAnimatorStateInfo(0).IsName("Player_Attack4"))
            horizontalValue = 0;
        else
            horizontalValue = Input.GetAxis("Horizontal");
    }
    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
    void Slide()
    {
        isSliding = true;
        animator.SetTrigger("Slide");
    }
    void SlideMechanic()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Player_Slide"))
        {
            //code which makes object move certain direction

            if (isFacingRight)
                horizontalValue = 1;
            else
                horizontalValue = -1;

            boxCollider2d.enabled = false;
            playerRigidbody.isKinematic = true;
            transform.Find("GroundCheck").GetComponent<BoxCollider2D>().enabled = false;
        }
        else
        {
            boxCollider2d.enabled = true;
            playerRigidbody.isKinematic = false;
            transform.Find("GroundCheck").GetComponent<BoxCollider2D>().enabled = true;
            //transform.Find("GroundCheck").gameObject.SetActive(true);
            isSliding = false;
        }
    }
    void TurningMechanic()
    {
        //code that manages character facing side
        if (horizontalValue > 0)
            isFacingRight = true;
        else if (horizontalValue < 0)
            isFacingRight = false;

        //code that turns character
        if (isFacingRight)
            transform.localScale = new Vector3(6, 6, 0);
        else
            transform.localScale = new Vector3(-6, 6, 0);
    }
    void RunningMechanic()
    {
        transform.position += new Vector3(horizontalValue, 0, 0) * Time.deltaTime * movementSpeed;

        //code which sets running animation
        if (horizontalValue != 0)
            animator.SetBool("IsRunning", true);
        else
            animator.SetBool("IsRunning", false);
    }
}
